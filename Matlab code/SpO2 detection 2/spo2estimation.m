clc; close all; clear all
fileID = fopen('data_ex_-12_R.txt','r');
formatSpec = '%f';
PPGrawFullRed = fscanf(fileID,formatSpec);

fileID = fopen('data_ex_-12_G.txt','r');
PPGrawFullGreen = fscanf(fileID,formatSpec);
figure; plot(PPGrawFullRed);
figure; plot(PPGrawFullGreen);


PPGrawRed = PPGrawFullRed(200:300);
figure; plot(PPGrawRed);
PPGrawGreen = PPGrawFullGreen(200:300);
figure; plot(PPGrawGreen);

t = 1:length(PPGrawRed);


PPGrawRed_detrended = detrending(PPGrawRed);
figure; plot(PPGrawRed_detrended);
PPGrawGreen_detrended = detrending(PPGrawGreen);
figure; plot(PPGrawGreen_detrended);

[ maximaRed, minimaRed ]     = peakDetect( PPGrawRed_detrended, 0.03, 15 );
[ maximaGreen, minimaGreen ] = peakDetect( PPGrawGreen_detrended, 0.03, 15 );


figure
hold on
plot(t,PPGrawRed_detrended)
plot(maximaRed,PPGrawRed_detrended(maximaRed),'rv','MarkerFaceColor','r')
plot(minimaRed,PPGrawRed_detrended(minimaRed),'rs','MarkerFaceColor','b')
axis([0 300 -1.1 1.1])
hold off;

figure
hold on
plot(t,PPGrawGreen_detrended)
plot(maximaGreen,PPGrawGreen_detrended(maximaGreen),'rv','MarkerFaceColor','r')
plot(minimaGreen,PPGrawGreen_detrended(minimaGreen),'rs','MarkerFaceColor','b')
axis([0 300 -1.1 1.1])
hold off;

coeffRed=polyfit((22:26),PPGrawRed_detrended(22:26)',1); 
coeffGreen=polyfit((22:26),PPGrawGreen_detrended(22:26)',1); 

slopeRed = coeffRed(1)
slopeGreen = coeffGreen(1)

heightRed = PPGrawRed_detrended(30) - PPGrawRed_detrended(18)
heightGreen = PPGrawGreen_detrended(30) - PPGrawGreen_detrended(18)