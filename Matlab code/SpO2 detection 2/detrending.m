function [ smoothData ] = detrending( signal )
% Detrending Data
    [p,s,mu] = polyfit((1:numel(signal))',signal,3);
    p
    f_y = polyval(p,(1:numel(signal))',[],mu);
    signal_data = signal - f_y;        % Detrend data
    figure;
    plot(f_y);

    smoothData = sgolayfilt(signal_data,7,21);
end

